package com.sebas.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sebas.model.User;

public interface UserDAO extends JpaRepository<User, Long> {
	
	User findOneByUsername(String username);
}